package com.kenny.concurrency.example.threadpool;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class ThreadPoolTest {


    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i=0; i<5;i++){
            final int j =i;
            executorService.execute(()->{
             log.info("runing {}",j);

           });
        }
        executorService.shutdown();
    }



}

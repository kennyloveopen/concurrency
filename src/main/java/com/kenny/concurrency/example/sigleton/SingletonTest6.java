package com.kenny.concurrency.example.sigleton;


import com.kenny.concurrency.anoations.NotRecommend;
import com.kenny.concurrency.anoations.NotThreadSafe;

/**
 * 饿汉模式
 */
@NotThreadSafe
@NotRecommend
public class SingletonTest6 {

    private SingletonTest6(){


    }

    private static SingletonTest6 instance;

    static{
        instance= new SingletonTest6();
    }

    public  static SingletonTest6 getInstance(){
        return  instance;
    }

    public  static void main(String[] args){
        System.out.println(getInstance().hashCode());
        System.out.println(getInstance().hashCode());
    }
}

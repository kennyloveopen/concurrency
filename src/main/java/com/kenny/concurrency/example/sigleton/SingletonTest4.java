package com.kenny.concurrency.example.sigleton;


import com.kenny.concurrency.anoations.NotThreadSafe;
import com.kenny.concurrency.anoations.ThreadSafe;

/**
 * 懒汉模式
 */
@ThreadSafe
public class SingletonTest4 {

    private SingletonTest4(){


    }

    /**
     * 1. 分布对象空间
     * 2.初始化对象
     * 3.设置instance 指向刚分配的内存
     */

    /**
     * JVM 和cpu 优化，指令重排
     *
     *  1. 分布对象空间
     *   * 3.设置instance 指向刚分配的内存
     *   * 2.初始化对象
     *
     *      */
    
    private volatile  static SingletonTest4 instance = null;
    public   static SingletonTest4 getInstance(){

        if(instance ==null){ //双重检测
            synchronized (SingletonTest4.class) {
                if(instance==null) {
                    instance = new SingletonTest4();

                }
                }
        }
        return  instance;
    }
}

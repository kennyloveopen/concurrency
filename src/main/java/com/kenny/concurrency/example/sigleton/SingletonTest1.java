package com.kenny.concurrency.example.sigleton;


import com.kenny.concurrency.anoations.NotThreadSafe;

/**
 * 懒汉模式
 */
@NotThreadSafe
public class SingletonTest1 {

    private SingletonTest1(){


    }

    private static SingletonTest1 instance = null;
    public   static  synchronized SingletonTest1 getInstance(){
        if(instance ==null){
            instance = new SingletonTest1();
        }
        return  instance;
    }
}

package com.kenny.concurrency.example.sigleton;


import com.kenny.concurrency.anoations.NotRecommend;
import com.kenny.concurrency.anoations.NotThreadSafe;
import com.kenny.concurrency.anoations.Recommend;
import com.kenny.concurrency.anoations.ThreadSafe;

/**
 * 饿汉模式
 */
@ThreadSafe
@Recommend
public class SingletonTest7 {

    private SingletonTest7(){


    }



    public  static SingletonTest7 getInstance(){
       return Singleton.INSTANCE.getInstance();
    }

    private enum Singleton{
        INSTANCE;
        private SingletonTest7 singleton;
        //JVM 保证只掉用一次
        Singleton(){
            singleton = new SingletonTest7();
        }

        private SingletonTest7 getInstance(){
            return singleton;
        }

    }

    public  static void main(String[] args){
        System.out.println(getInstance().hashCode());
        System.out.println(getInstance().hashCode());
    }
}

package com.kenny.concurrency.example.sigleton;


import com.kenny.concurrency.anoations.NotThreadSafe;

/**
 * 懒汉模式
 */
@NotThreadSafe
public class SingletonTest3 {

    private SingletonTest3(){


    }

    /**
     * 1. 分布对象空间
     * 2.初始化对象
     * 3.设置instance 指向刚分配的内存
     */

    /**
     * JVM 和cpu 优化，指令重排
     *
     *  1. 分布对象空间
     *   * 3.设置instance 指向刚分配的内存
     *   * 2.初始化对象
     *
     *      */

    private static SingletonTest3 instance = null;
    public   static   SingletonTest3 getInstance(){

        if(instance ==null){ //双重检测
            synchronized (SingletonTest3.class) {
                if(instance==null) {
                    instance = new SingletonTest3();

                }
                }
        }
        return  instance;
    }
}

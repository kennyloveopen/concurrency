package com.kenny.concurrency.example.sigleton;


import com.kenny.concurrency.anoations.NotRecommend;
import com.kenny.concurrency.anoations.NotThreadSafe;

/**
 * 饿汉模式
 */
@NotThreadSafe
@NotRecommend
public class SingletonTest2 {

    private SingletonTest2(){


    }

    private static SingletonTest2 instance = new SingletonTest2();
    public  static SingletonTest2 getInstance(){
        return  instance;
    }
}

package com.kenny.concurrency.example.publish;

import com.kenny.concurrency.anoations.NotThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
@NotThreadSafe
public class UnsafePublish {

    public String[] stats={"a","b","c"};


    public String[] getStats() {
        return stats;
    }

    public static void main(String[] args){
        UnsafePublish  unsafePublish = new UnsafePublish();
        log.info("{}", Arrays.toString(unsafePublish.getStats()));
        unsafePublish.getStats()[0] ="d";
        log.info("{}", Arrays.toString(unsafePublish.getStats()));
    }
}

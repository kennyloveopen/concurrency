package com.kenny.concurrency.example.atomic;

import com.kenny.concurrency.anoations.NotThreadSafe;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by apple on 2018/5/24.
 */
@Slf4j
@NotThreadSafe
public class AtomicTest5 {
    @Getter
    public volatile  int count =100;

   public static AtomicIntegerFieldUpdater<AtomicTest5> update= AtomicIntegerFieldUpdater.newUpdater(AtomicTest5.class, "count");

    public static AtomicTest5 atomicTest5 = new AtomicTest5();
    public static void main(String[] args) throws InterruptedException {
      if(update.compareAndSet(atomicTest5,100,120)){
          log.info("update success {}", atomicTest5.getCount());

      }

        if(update.compareAndSet(atomicTest5,100,120)){
            log.info("update success2 {}", atomicTest5.getCount());

        }else {
            log.info("update failed");
        }

    }

    }



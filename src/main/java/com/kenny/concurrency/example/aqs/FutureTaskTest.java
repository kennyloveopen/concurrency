package com.kenny.concurrency.example.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

@Slf4j
public class FutureTaskTest {

    static class MyCallable implements Callable<String>{


        @Override
        public String call() throws Exception {

            log.info("do something in callable");
            Thread.sleep(5000);
            return "Done";
        }
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        FutureTask<String> futureTask = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                log.info("do something in callable");
                Thread.sleep(5000);
                return "Done";
            }
        }

        );

        new Thread(futureTask).start();

        log.info("do something in main");
        Thread.sleep(1000);
        String reslut = futureTask.get();
        log.info("result:{}",reslut);

    }
}

package com.kenny.concurrency.example.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class CyclicBarrierTest1 {

    private static int threadCount=10;
    static  final CyclicBarrier cyclicBarrier = new CyclicBarrier(5);
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();

        for(int i=0;i<threadCount;i++){
            final int threadNum=i;
            Thread.sleep(1000);
            executorService.execute(()->{

                try {
                    race(threadNum);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        log.info("finish");
        executorService.shutdown();

    }

    private static void race (int threadNum) throws Exception {
        Thread.sleep(1000);
            log.info("ready ===== {}",threadNum);
            try {
                cyclicBarrier.await(2000, TimeUnit.MILLISECONDS);
            }catch (Exception e){
                log.warn("BrokException", e);
            }
            log.info("continue===={}",threadNum);


    }

}

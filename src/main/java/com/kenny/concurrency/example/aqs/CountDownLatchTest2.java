package com.kenny.concurrency.example.aqs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

@Slf4j
public class CountDownLatchTest2 {

    private static int threadCount=200;

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(3);
        for(int i=0;i<threadCount;i++){
            final int threadNum=i;
            executorService.execute(()->{
                try {
                    semaphore.acquire();
                    test(threadNum);
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                }

            });
        }
        log.info("finish");
        executorService.shutdown();

    }

    private static void test (int threadNum) throws InterruptedException {


            log.info("{}",threadNum);
        Thread.sleep(1000);


    }

}

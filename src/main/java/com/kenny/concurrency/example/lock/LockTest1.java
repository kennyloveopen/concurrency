package com.kenny.concurrency.example.lock;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j
public class LockTest1 {

    public  final Map <String,Data> map = new TreeMap<>();
    public static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public  final  Lock readLock = lock.readLock();
    public final Lock writeLock = lock.writeLock();

    public Data get(String key){
        readLock.lock();
        try{
            return map.get(key);
        }finally {
            readLock.unlock();
        }

    }
    public Data put(String key,Data data){
        writeLock.lock();
        try{
            return map.put(key,data);
        }finally {
            writeLock.unlock();
        }

    }

    public Set<String> getAllKeys(){
        readLock.lock();
        try{
            return map.keySet();
        }finally {
            readLock.unlock();
        }


    }

    public static void main(String[] args) throws InterruptedException {

    }


    class Data{

    }

}

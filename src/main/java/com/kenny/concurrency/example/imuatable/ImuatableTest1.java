package com.kenny.concurrency.example.imuatable;

import com.google.common.collect.Maps;

import java.util.Map;

public class ImuatableTest1 {

    private final static Integer a=0;

    private final  static String b="2";
    private final  static Map<Integer,Integer> map = Maps.newHashMap();


    static {
        map.put(1,2);
        map.put(2,3);

    }

    public static void main(String[] args) {
        map.put(3,4);
        System.out.println(map);
    }

    public final void test(final int a){
//        a=1;
    }
}

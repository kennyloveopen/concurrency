package com.kenny.concurrency.example.imuatable;

import com.google.common.collect.Maps;
import com.kenny.concurrency.anoations.ThreadSafe;

import java.util.Collections;
import java.util.Map;

@ThreadSafe
public class ImuatableTest2 {


    private   static Map<Integer,Integer> map = Maps.newHashMap();


    static {
        map.put(1,2);
        map.put(2,3);
        map= Collections.unmodifiableMap(map);

    }

    public static void main(String[] args) {
        map.put(3,4);
        System.out.println(map);
    }


}

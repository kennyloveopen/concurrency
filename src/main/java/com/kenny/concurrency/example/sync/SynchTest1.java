package com.kenny.concurrency.example.sync;

import com.kenny.concurrency.anoations.NotThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by apple on 2018/5/24.
 */
@Slf4j
@NotThreadSafe
public class SynchTest1 {


    public void test1(int j){
        synchronized (this){
            for(int i=0;i<10;i++){
                log.info("test1-{}--:{}",j,i);
            }
        }
    }

    public synchronized void test2(int j){
        for(int i=0;i<10;i++){
            log.info("test2-{}--:{}",j, i);
        }
    }

    public static void main(String[] args){
        SynchTest1 synchTest1 = new SynchTest1();
        SynchTest1 synchTest2 = new SynchTest1();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(()->{
            synchTest1.test2(1);
        });
        executorService.execute(()->{
            synchTest2.test2(2);
        });


    }
}

package com.kenny.concurrency.example.sync;

import com.kenny.concurrency.anoations.NotThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by apple on 2018/5/24.
 */
@Slf4j
@NotThreadSafe
public class SynchTest2 {


    public static void  test1(int j){
        synchronized (SynchTest1.class){
            for(int i=0;i<10;i++){
                log.info("test1-{}--:{}",j,i);
            }
        }
    }

    public synchronized static  void test2(int j){
        for(int i=0;i<10;i++){
            log.info("test2-{}--:{}",j, i);
        }
    }

    public static void main(String[] args){
        SynchTest2 synchTest1 = new SynchTest2();
        SynchTest2 synchTest2 = new SynchTest2();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(()->{
            synchTest1.test1(1);
        });
        executorService.execute(()->{
            synchTest2.test1(2);
        });


    }
}
